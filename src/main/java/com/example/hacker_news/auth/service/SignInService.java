package com.example.hacker_news.auth.service;

import com.example.hacker_news.auth.model.request.SignInRequest;

public interface SignInService {

    void signIn(SignInRequest signInRequest);

}
