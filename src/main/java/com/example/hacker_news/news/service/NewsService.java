package com.example.hacker_news.news.service;

import com.example.hacker_news.news.data.entity.NewsEntity;
import com.example.hacker_news.news.model.request.NewsQueryParams;
import com.example.hacker_news.news.model.response.NewsPage;
import com.example.hacker_news.newssource.model.response.NewsResponseWrapper;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@Validated
public interface NewsService {

    List<NewsEntity> storeNews(@NotNull NewsResponseWrapper responseWrapper);

    NewsPage getNews(@NotNull @Valid NewsQueryParams params);

    void deleteNews(@NotNull UUID newsId);
}
