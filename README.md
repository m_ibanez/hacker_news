# Hacker News API

This project is a API that lets you query the latest top articles from Hacker News

Each hour the API will load the latest 20 news to a Postgres DB so that they can be queried at any moment!!!

## Technologies: 

The project uses the next technologies:

1. Spring Boot
2. Maven
3. Swagger
4. JWT
5. JPA
6. Flyway

## Installation

To install the project you need to execute two steps:

1. Create a docker image for the API
2. Use docker compose to run the API in a container

After that you will find how to access to the API endpoints at [this route](http://localhost:8080/swagger-ui/).

You can also use [this Postman collection](https://www.getpostman.com/collections/03042763a0ea83492bf1) to consult the available endpoints with the preloaded data. This collection contains the base user and some parameters to filter the news.  

#### 1. Create a docker image for the API

To create the docker image you need to run the next command:

```mvn clean package docker:build```

That will create an image with the name `docker-spring-boot-app`

After that you need to execute the next step

#### 2. Use docker compose to run the API in a container

At the root of the project is the `docker-compose.yml` file so all you need to do is run the command:

```docker-compose up```
