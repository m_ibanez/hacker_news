package com.example.hacker_news.auth.service;

import com.example.hacker_news.auth.model.request.AuthRequest;
import com.example.hacker_news.auth.model.response.AuthResponse;

public interface AuthenticationService {

    AuthResponse authenticate(AuthRequest authRequest);

}
