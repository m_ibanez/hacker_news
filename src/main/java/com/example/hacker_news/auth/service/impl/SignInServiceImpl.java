package com.example.hacker_news.auth.service.impl;

import com.example.hacker_news.auth.data.entity.UserEntity;
import com.example.hacker_news.auth.data.repository.UserRepository;
import com.example.hacker_news.auth.model.request.SignInRequest;
import com.example.hacker_news.auth.service.SignInService;
import com.example.hacker_news.exceptions.PasswordMustBeSameException;
import com.example.hacker_news.exceptions.UserAlreadyExistsException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class SignInServiceImpl implements SignInService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public void signIn(SignInRequest signInRequest) {
        checkPassword(signInRequest);
    }

    private void checkPassword(SignInRequest signInRequest) {
        if (!signInRequest.getPassword().equals(signInRequest.getRepeatPassword())) {
            throw new PasswordMustBeSameException();
        }
        if (userRepository.existsById(signInRequest.getUsername())) {
            throw new UserAlreadyExistsException(signInRequest.getUsername());
        }
        userRepository.save(mapEntity(signInRequest));
    }

    private UserEntity mapEntity(SignInRequest signInRequest) {
        return new UserEntity(
                signInRequest.getUsername(),
                passwordEncoder.encode(signInRequest.getPassword()),
                LocalDateTime.now()
        );
    }

}
