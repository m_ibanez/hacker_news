package com.example.hacker_news.news.model.request;

import com.example.hacker_news.news.data.entity.NewsEntity;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Objects;
import java.util.stream.Stream;

@Data
public class NewsQueryParamsResolver {

    private CriteriaBuilder cb;
    private Root<NewsEntity> root;
    private Predicate author;
    private Predicate tags;
    private Predicate title;
    private Predicate dates;
    private Predicate deleted;

    public NewsQueryParamsResolver(NewsQueryParams params, CriteriaBuilder cb, Root<NewsEntity> root) {
        this.cb = cb;
        this.root = root;
        this.author = resolveItem(params.getAuthor(), "author");
        this.title = resolveItem(params.getTitle(), "title");
        this.tags = resolveItem(params.getTags(), "tags");
        this.dates = resolveDates(params.getMonth());
        this.deleted = getDeletedPredicate();
    }

    private Predicate getDeletedPredicate() {
        return cb.equal(root.get("deleted"), false);
    }

    private Predicate resolveItem(String value, String field) {
        if (StringUtils.isNotBlank(value))
            return cb.like(cb.lower(root.get(field)), "%" + value.toLowerCase() + "%");
        return null;
    }

    private Predicate resolveDates(String month) {
        if (StringUtils.isNotBlank(month)) {
            LocalDateTime pivot = LocalDate.now()
                    .atStartOfDay()
                    .withMonth(Month.valueOf(month.toUpperCase()).getValue());
            LocalDateTime startDate = pivot.withDayOfMonth(1);
            LocalDateTime endDate = startDate.plusMonths(1);
            return cb.between(root.get("createdAt"), startDate, endDate);
        }
        return null;
    }

    public Predicate[] getAllPredicates() {
        return Stream.of(author, title, tags, dates, deleted)
                .filter(Objects::nonNull).toArray(Predicate[]::new);
    }

}
