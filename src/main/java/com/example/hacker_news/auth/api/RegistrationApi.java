package com.example.hacker_news.auth.api;

import com.example.hacker_news.auth.model.request.SignInRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Api(value = "Registration API", tags = "Sign In")
@Validated
@RequestMapping("/sign-in")
public interface RegistrationApi {

    @ApiOperation(value = "POST credentials for registration", tags = "Sign In")
    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    void signIn(@RequestBody @NotNull @Valid SignInRequest signInRequest);

}
