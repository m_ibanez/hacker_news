package com.example.hacker_news.common.model.response;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class ApiErrorResponse {

    private LocalDateTime executionTime;
    private Integer statusCode;
    private String reason;
    private List<String> messages;

    public ApiErrorResponse(HttpStatus status, String reason, List<String> messages) {
        this.executionTime = LocalDateTime.now();
        this.statusCode = status.value();
        this.reason = reason;
        this.messages = messages;
    }

}
