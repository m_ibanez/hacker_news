package com.example.hacker_news.news.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "news", schema = "hacker_news")
@ToString
public class NewsEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID newsId;
    private Long storyId;
    private String author;
    private String title;
    private String url;
    private Long points;
    private String text;
    private String comment;
    private Integer commentNumber;
    private Long parentId;
    @Column(name = "object_id")
    private Long objectID;
    private LocalDateTime createdAt;
    private String tags;
    private boolean deleted;

}
