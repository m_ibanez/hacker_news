package com.example.hacker_news.auth.service.impl;

import com.example.hacker_news.auth.data.entity.UserEntity;
import com.example.hacker_news.auth.data.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findById(username)
                .map(this::toUserDetails)
                .orElseThrow(() -> new UsernameNotFoundException(username));
    }

    private UserDetails toUserDetails(UserEntity userEntity) {
        return new User(userEntity.getUsername(), userEntity.getPassword(), new ArrayList<>());
    }

}
