package com.example.hacker_news.news.model.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
public class NewsStory {
    @ApiModelProperty(name = "newsId", example = "e1122b63-18b2-491a-9708-157c0e9f093e")
    private UUID newsId;
    @ApiModelProperty(name = "storyId", example = "33639103")
    private Long storyId;
    @ApiModelProperty(name = "author", example = "duped")
    private String author;
    @ApiModelProperty(name = "title", example = "FTX used corporate funds to purchase employee homes, new filing shows")
    private String title;
    @ApiModelProperty(name = "url", example = "https://www.cnbc.com/2022/11/17/ftx-used-corporate-funds-to-purchase-employee-homes-new-filing-shows.html")
    private String url;
    @ApiModelProperty(name = "author", example = "1000")
    private Long points;
    @ApiModelProperty(name = "text", example = "Some large text that may be null")
    private String text;
    @ApiModelProperty(name = "comment", example = "Some large comment that may be null")
    private String comment;
    @ApiModelProperty(name = "commentNumber", example = "1")
    private Integer commentNumber;
    @ApiModelProperty(name = "parentId", example = "33644327")
    private Long parentId;
    @ApiModelProperty(name = "objectID", example = "33649298")
    private Long objectID;
    @ApiModelProperty(name = "createdAt", example = "2022-11-18T02:47:05")
    private LocalDateTime createdAt;
    @ApiModelProperty(name = "tags", example = "comment,author_duped,story_33639103")
    private String tags;
}
