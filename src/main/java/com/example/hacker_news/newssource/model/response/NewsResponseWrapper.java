package com.example.hacker_news.newssource.model.response;

import lombok.Data;

import java.util.List;

@Data
public class NewsResponseWrapper {
    private List<NewsItem> hits;
}
