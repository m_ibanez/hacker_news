package com.example.hacker_news.auth.model.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class SignInRequest {
    @ApiModelProperty(value = "username", required = true, example = "user@api.com")
    @NotEmpty
    private String username;
    @ApiModelProperty(value = "password", required = true, example = "50m3P455w0rD")
    @NotEmpty
    private String password;
    @ApiModelProperty(value = "repeatPassword", required = true, example = "50m3P455w0rD")
    @NotEmpty
    private String repeatPassword;
}
