package com.example.hacker_news.newssource.scheduler;

import com.example.hacker_news.news.data.entity.NewsEntity;
import com.example.hacker_news.news.service.NewsService;
import com.example.hacker_news.newssource.service.NewsSourceService;
import com.example.hacker_news.newssource.model.response.NewsResponseWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class NewsExtractionScheduler {

    private final NewsSourceService newsSourceService;
    private final NewsService newsService;

    @Scheduled(cron = "${hacker-news.extraction-cron}")
    public void extractHackerNews() {
        log.info("Started extraction at {}", LocalDateTime.now());
        NewsResponseWrapper responseWrapper = newsSourceService.getLatestNews();
        log.info("Got {} latest news", responseWrapper.getHits().size());
        List<NewsEntity> storedNews = newsService.storeNews(responseWrapper);
        log.info("Saved {} news", storedNews.size());
        log.info("Finished extraction at {}", LocalDateTime.now());
    }

}
