package com.example.hacker_news.auth.model.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthResponse {
    @ApiModelProperty(name = "jwtToken", example = "token")
    private String jwtToken;
}
