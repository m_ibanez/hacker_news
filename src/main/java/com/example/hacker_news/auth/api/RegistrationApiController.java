package com.example.hacker_news.auth.api;

import com.example.hacker_news.auth.model.request.SignInRequest;
import com.example.hacker_news.auth.service.SignInService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class RegistrationApiController implements RegistrationApi {

    private final SignInService signInService;

    @Override
    public void signIn(SignInRequest signInRequest) {
        signInService.signIn(signInRequest);
    }

}
