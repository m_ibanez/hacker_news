package com.example.hacker_news.newssource.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class NewsItem {
    @JsonProperty("created_at")
    private LocalDateTime createdAt;
    private String title;
    private String url;
    private String author;
    private Long points;
    @JsonProperty("story_text")
    private String storyText;
    @JsonProperty("comment_text")
    private String commentText;
    @JsonProperty("num_comments")
    private Integer commentNumber;
    @JsonProperty("story_id")
    private Long storyId;
    @JsonProperty("story_title")
    private String storyTitle;
    @JsonProperty("story_url")
    private String storyUrl;
    @JsonProperty("_tags")
    private List<String> tags;
    private Long objectID;
    @JsonProperty("parent_id")
    private Long parentId;
}
