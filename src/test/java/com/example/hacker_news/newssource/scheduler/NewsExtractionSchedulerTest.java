package com.example.hacker_news.newssource.scheduler;

import base.BaseServiceTest;
import com.example.hacker_news.news.data.repository.NewsRepository;
import com.example.hacker_news.news.service.NewsService;
import com.example.hacker_news.newssource.service.NewsSourceService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NewsExtractionSchedulerTest extends BaseServiceTest {

    @Autowired
    private NewsSourceService newsSourceService;
    @Autowired
    private NewsService newsService;
    @Autowired
    private NewsRepository newsRepository;

    private NewsExtractionScheduler newsExtractionScheduler;

    @BeforeEach
    void setup() {
        newsExtractionScheduler = new NewsExtractionScheduler(newsSourceService, newsService);
    }

    @Test
    void shouldSaveNews() {
        newsExtractionScheduler.extractHackerNews();
        assertEquals(25, newsRepository.findAll().size());
    }
}