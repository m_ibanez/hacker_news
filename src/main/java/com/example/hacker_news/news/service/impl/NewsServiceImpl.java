package com.example.hacker_news.news.service.impl;

import com.example.hacker_news.exceptions.NewsNotFoundException;
import com.example.hacker_news.news.data.entity.NewsEntity;
import com.example.hacker_news.news.data.repository.NewsRepository;
import com.example.hacker_news.news.model.request.NewsQueryParams;
import com.example.hacker_news.news.model.request.NewsQueryParamsResolver;
import com.example.hacker_news.news.model.response.NewsPage;
import com.example.hacker_news.news.model.response.NewsStory;
import com.example.hacker_news.news.service.NewsService;
import com.example.hacker_news.newssource.model.response.NewsItem;
import com.example.hacker_news.newssource.model.response.NewsResponseWrapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.springframework.beans.BeanUtils.copyProperties;

@Slf4j
@Service
@RequiredArgsConstructor
public class NewsServiceImpl implements NewsService {

    private static final Integer MAX_PAGE_SIZE = 5;

    private final NewsRepository newsRepository;
    private final EntityManager entityManager;

    @Override
    public List<NewsEntity> storeNews(NewsResponseWrapper responseWrapper) {
        List<NewsEntity> itemsToSave = mapEntities(responseWrapper);
        return newsRepository.saveAll(itemsToSave);
    }

    @Override
    public NewsPage getNews(NewsQueryParams params) {
        List<NewsEntity> content = listNews(params);
        Long totalItems = countNews(params);
        return NewsPage.builder()
                .totalItems(totalItems)
                .page(params.getPage())
                .news(fromEntities(content))
                .build();
    }

    @Override
    public void deleteNews(UUID newsId) {
        NewsEntity entity = newsRepository.findById(newsId)
                .orElseThrow(() -> new NewsNotFoundException(newsId));
        entity.setDeleted(true);
        newsRepository.save(entity);
    }

    private List<NewsEntity> listNews(NewsQueryParams params) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<NewsEntity> cq = cb.createQuery(NewsEntity.class);
        Root<NewsEntity> root = cq.from(NewsEntity.class);
        NewsQueryParamsResolver resolver = new NewsQueryParamsResolver(params, cb, root);
        Predicate[] whereParams = resolver.getAllPredicates();
        cq.select(root).where(whereParams);
        return entityManager.createQuery(cq)
                .setMaxResults(MAX_PAGE_SIZE)
                .setFirstResult(MAX_PAGE_SIZE * (params.getPage() - 1))
                .getResultList();
    }

    private Long countNews(NewsQueryParams params) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<NewsEntity> root = cq.from(NewsEntity.class);
        NewsQueryParamsResolver resolver = new NewsQueryParamsResolver(params, cb, root);
        Predicate[] whereParams = resolver.getAllPredicates();
        cq.select(cb.count(root)).where(whereParams);
        return entityManager.createQuery(cq)
                .getSingleResult();
    }

    private List<NewsStory> fromEntities(List<NewsEntity> entities) {
        return entities.stream().map(entity -> {
                    NewsStory target = new NewsStory();
                    copyProperties(entity, target);
                    return target;
                })
                .collect(Collectors.toList());
    }

    private List<NewsEntity> mapEntities(NewsResponseWrapper responseWrapper) {
        List<NewsEntity> entities = new ArrayList<>();
        for (NewsItem item : responseWrapper.getHits()) {
            entities.add(mapEntity(item));
        }
        return entities;
    }

    private NewsEntity mapEntity(NewsItem newsItem) {
        NewsEntity entity = new NewsEntity();
        entity.setStoryId(newsItem.getStoryId());
        entity.setTitle(getNonBlank(newsItem.getTitle(), newsItem.getStoryTitle()));
        entity.setUrl(getNonBlank(newsItem.getUrl(), newsItem.getStoryUrl()));
        entity.setAuthor(newsItem.getAuthor());
        entity.setPoints(newsItem.getPoints());
        entity.setText(newsItem.getStoryText());
        entity.setComment(newsItem.getCommentText());
        entity.setCommentNumber(newsItem.getCommentNumber());
        entity.setParentId(newsItem.getParentId());
        entity.setObjectID(newsItem.getObjectID());
        entity.setCreatedAt(newsItem.getCreatedAt());
        entity.setTags(String.join(",", newsItem.getTags()));
        log.info("Mapped entity {}", entity);
        return entity;
    }

    private String getNonBlank(String left, String right) {
        return StringUtils.isNotBlank(left) ? left : right;
    }

}
