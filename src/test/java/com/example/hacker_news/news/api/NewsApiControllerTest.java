package com.example.hacker_news.news.api;

import base.BaseIntegrationTest;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class NewsApiControllerTest extends BaseIntegrationTest {

    @SneakyThrows
    @Test
    void shouldThrowUnauthorizedWhenNoAuthHeaderIsPresent() {
        mockMvc.perform(get("/news"))
                .andExpect(status().isUnauthorized())
                .andDo(print());
    }

    @SneakyThrows
    @Test
    void shouldThrowBadRequest() {
        mockMvc.perform(get("/news")
                        .header("Authorization", formatToken()))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @SneakyThrows
    @Test
    void shouldReturnOk() {
        mockMvc.perform(get("/news")
                        .param("page", "0")
                        .header("Authorization", formatToken()))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @SneakyThrows
    @Test
    void shouldFilterByTags() {
        mockMvc.perform(get("/news")
                        .param("tags", "author_statico")
                        .param("page", "0")
                        .header("Authorization", formatToken()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.news.[0].tags").value("comment,author_statico,story_33635218"))
                .andDo(print());
    }

    @SneakyThrows
    @Test
    void shouldFilterByAuthor() {
        mockMvc.perform(get("/news")
                        .param("author", "statico")
                        .param("page", "0")
                        .header("Authorization", formatToken()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.news.[0].author").value("statico"))
                .andDo(print());
    }

    @SneakyThrows
    @Test
    void shouldFilterByTitle() {
        mockMvc.perform(get("/news")
                        .param("title", "Good resource on writing")
                        .param("page", "0")
                        .header("Authorization", formatToken()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.news.[0].title").value("Ask HN: Good resource on writing web app with plain JavaScript/HTML/CSS"))
                .andDo(print());
    }

    @SneakyThrows
    @Test
    void shouldFilterByMonth() {
        mockMvc.perform(get("/news")
                        .param("month", "november")
                        .param("page", "0")
                        .header("Authorization", formatToken()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.news.[0].title").value("Ask HN: Good resource on writing web app with plain JavaScript/HTML/CSS"))
                .andDo(print());
    }

    @SneakyThrows
    @Test
    void shouldThrowNptFoundWhenDeleting() {
        mockMvc.perform(delete("/news/{id}", UUID.randomUUID().toString())
                        .header("Authorization", formatToken()))
                .andExpect(status().isNotFound())
                .andDo(print());
    }

}