package com.example.hacker_news.news.api;

import com.example.hacker_news.news.model.request.NewsQueryParams;
import com.example.hacker_news.news.model.response.NewsPage;
import com.example.hacker_news.news.service.NewsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class NewsApiController implements NewsApi {

    private final NewsService newsService;

    @Override
    public NewsPage getNews(NewsQueryParams params) {
        return newsService.getNews(params);
    }

    @Override
    public void delete(UUID newsId) {
        newsService.deleteNews(newsId);
    }

}
