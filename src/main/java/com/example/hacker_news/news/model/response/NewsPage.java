package com.example.hacker_news.news.model.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NewsPage {

    @ApiModelProperty(name = "totalItems", example = "1")
    private Long totalItems;
    @ApiModelProperty(name = "page", example = "1")
    private Integer page;
    @ApiModelProperty(name = "news")
    private List<NewsStory> news;

}
