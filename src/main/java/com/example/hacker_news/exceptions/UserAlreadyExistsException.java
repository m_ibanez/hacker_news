package com.example.hacker_news.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.constraints.NotEmpty;

public class UserAlreadyExistsException extends ResponseStatusException {
    public UserAlreadyExistsException(@NotEmpty String username) {
        super(HttpStatus.CONFLICT, String.format("The user %s already exists", username));
    }
}
