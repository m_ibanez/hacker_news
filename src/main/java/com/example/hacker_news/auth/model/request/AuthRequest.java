package com.example.hacker_news.auth.model.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class AuthRequest {

    @ApiModelProperty(value = "username", required = true, example = "user@api.com")
    @NotEmpty(message = "The username must not be empty")
    private String username;
    @ApiModelProperty(value = "password", required = true, example = "50m3P455w0rD")
    @NotEmpty(message = "The password must not be empty")
    private String password;

}
