CREATE SCHEMA IF NOT EXISTS hacker_news;

DROP TABLE IF EXISTS hacker_news.user;
CREATE TABLE hacker_news.user
(
    username   varchar(60)  NOT NULL,
    password   varchar(100) NOT NULL,
    created_at TIMESTAMP    NOT NULL,
    CONSTRAINT user_pkey PRIMARY KEY (username)
);

DROP TABLE IF EXISTS hacker_news.news;
CREATE TABLE hacker_news.news
(
    news_id uuid NOT NULL,
    story_id integer NULL,
    author varchar(50) NULL,
    title varchar(200) NULL,
    url varchar(200) NULL,
    points integer NULL,
    text varchar NULL,
    comment varchar NULL,
    comment_number integer NULL,
    parent_id integer NULL,
    object_id integer NULL,
    created_at timestamp    NOT NULL,
    tags varchar NOT NULL,
    deleted boolean NOT NULL,
    CONSTRAINT news_pkey PRIMARY KEY (news_id)
);
