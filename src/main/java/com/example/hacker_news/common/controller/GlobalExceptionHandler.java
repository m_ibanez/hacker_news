package com.example.hacker_news.common.controller;

import com.example.hacker_news.common.model.response.ApiErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {BindException.class})
    public ApiErrorResponse handleBindException(BindException ex) {
        List<String> messages = ex.getBindingResult().getFieldErrors()
                .stream().map(fieldError -> String.format("Param %s: %s", fieldError.getField(), fieldError.getDefaultMessage()))
                .collect(Collectors.toList());
        return new ApiErrorResponse(HttpStatus.BAD_REQUEST, "Invalid parameter", messages);
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(value = {BadCredentialsException.class})
    public ApiErrorResponse handleBadCredentialsException(BadCredentialsException ex) {
        return new ApiErrorResponse(HttpStatus.BAD_REQUEST, "Invalid credentials", Collections.singletonList(ex.getMessage()));
    }

    @ExceptionHandler(value = {ResponseStatusException.class})
    public ResponseEntity<ApiErrorResponse> handleAllResponseStatusException(ResponseStatusException ex) {
        return ResponseEntity.status(ex.getStatus())
                .body(new ApiErrorResponse(ex.getStatus(), ex.getStatus().getReasonPhrase(),
                        Collections.singletonList(ex.getReason())));
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = {Exception.class})
    public ApiErrorResponse handleException(Exception ex) {
        log.error("Exception occurred", ex);
        return new ApiErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(),
                Collections.singletonList("An error occurred while processing the request, please try again later"));
    }

}
