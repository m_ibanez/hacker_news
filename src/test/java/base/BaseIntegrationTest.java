package base;

import com.example.hacker_news.auth.model.request.AuthRequest;
import com.example.hacker_news.auth.model.response.AuthResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@AutoConfigureMockMvc
public class BaseIntegrationTest extends BaseServiceTest {

    @Autowired
    protected MockMvc mockMvc;

    protected String token;

    @BeforeEach
    void generateToken() throws Exception {
        AuthRequest request = new AuthRequest();
        request.setUsername("user@test.com");
        request.setPassword("50M3p455woRd");

        String auth = mockMvc.perform(post("/authenticate")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(request)))
                .andReturn()
                .getResponse().getContentAsString();
        AuthResponse response = objectMapper.readValue(auth, AuthResponse.class);
        token = response.getJwtToken();
    }

    public String formatToken() {
        return "Bearer " + token;
    }

}
