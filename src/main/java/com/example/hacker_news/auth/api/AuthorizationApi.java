package com.example.hacker_news.auth.api;

import com.example.hacker_news.auth.model.request.AuthRequest;
import com.example.hacker_news.auth.model.response.AuthResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Api(value = "Authorization API", tags = "Authorization")
@Validated
@RequestMapping("/authenticate")
public interface AuthorizationApi {

    @ApiOperation(value = "POST credentials for authentication", tags = "Authorization")
    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    AuthResponse authenticate(@RequestBody @NotNull @Valid AuthRequest authRequest);

}
