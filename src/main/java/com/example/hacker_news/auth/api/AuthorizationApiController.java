package com.example.hacker_news.auth.api;

import com.example.hacker_news.auth.model.request.AuthRequest;
import com.example.hacker_news.auth.model.response.AuthResponse;
import com.example.hacker_news.auth.service.AuthenticationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AuthorizationApiController implements AuthorizationApi {

    private final AuthenticationService authenticationService;

    @Override
    public AuthResponse authenticate(AuthRequest authRequest) {
        return authenticationService.authenticate(authRequest);
    }

}
