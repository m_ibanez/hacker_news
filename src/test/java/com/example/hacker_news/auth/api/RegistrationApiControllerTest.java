package com.example.hacker_news.auth.api;

import base.BaseIntegrationTest;
import com.example.hacker_news.auth.model.request.AuthRequest;
import com.example.hacker_news.auth.model.request.SignInRequest;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class RegistrationApiControllerTest extends BaseIntegrationTest {

    @SneakyThrows
    @Test
    void shouldSignInUserAndAuthenticate() {
        SignInRequest signInRequest = new SignInRequest();
        signInRequest.setUsername("some.user@test.com");
        signInRequest.setPassword("somePassW0RD_for73571nG");
        signInRequest.setRepeatPassword("somePassW0RD_for73571nG");
        mockMvc.perform(post("/sign-in")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(signInRequest)))
                .andExpect(status().isCreated())
                .andDo(print());
        AuthRequest authRequest = new AuthRequest();
        authRequest.setUsername(signInRequest.getUsername());
        authRequest.setPassword(signInRequest.getPassword());
        mockMvc.perform(post("/authenticate")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(authRequest)))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @SneakyThrows
    @Test
    void shouldThrowConflict() {
        SignInRequest signInRequest = new SignInRequest();
        signInRequest.setUsername("user@test.com");
        signInRequest.setPassword("somePassW0RD_for73571nG");
        signInRequest.setRepeatPassword("somePassW0RD_for73571nG");
        mockMvc.perform(post("/sign-in")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(signInRequest)))
                .andExpect(status().isConflict())
                .andDo(print());
    }

    @SneakyThrows
    @Test
    void shouldThrowUnprocessableEntity() {
        SignInRequest signInRequest = new SignInRequest();
        signInRequest.setUsername("some.user@test.com");
        signInRequest.setPassword("somePassW0RD_for73571nG");
        signInRequest.setRepeatPassword("somePassW0RD");
        mockMvc.perform(post("/sign-in")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(signInRequest)))
                .andExpect(status().isUnprocessableEntity())
                .andDo(print());
    }

}