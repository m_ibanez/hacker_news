truncate table hacker_news.user;

insert into hacker_news.user
values ('user@test.com', '$2a$10$isABmK17DYpVrfvt5xojAeJYYT2ClLKpNCvKVvvTKyD7aTwLdSDUW',
        '2022-11-17 02:06:30.04366');

truncate table hacker_news.news;

insert into hacker_news.news(news_id, story_id, author, title, comment, parent_id, object_id, created_at, tags, deleted)
values ('e1122b63-18b2-491a-9708-157c0e9f093e', 33635218, 'gal_anonym',
        'Ask HN: Good resource on writing web app with plain JavaScript/HTML/CSS',
        'For simple web &quot;app&quot; where you are not using dynamic reload of page elements, you can drop JavaScript and go with PHP + HTML + CSS. Use vanilla procedural PHP with sane splitting in functions &#x2F; files. This stack is good enough for simple web apps like for example task list etc. You can optionally sprinkle it with some vanilla JavaScript for dynamic parts.',
        33635218, 33635850, '2022-11-17 07:00:12', 'comment,author_gal_anonym,story_33635218', false),
       ('7250381d-7978-4dbe-a1fd-a7c93fbd6dac', 33635218, 'javaunsafe2019',
        'Ask HN: Good resource on writing web app with plain JavaScript/HTML/CSS',
        'I guess you will just end up writing your own framework in the end. There is a reason web framework exist - they all satisfy different needs in the domain of web development. Like routing, content update, state handling, event handling and so on. \nIf you don\’t need any of that, great, you can just learn and write plain html, css.\nSo LMGTFY <a href=""https:&#x2F;&#x2F;dev.to&#x2F;jordanholtdev&#x2F;how-to-create-a-web-page-with-html-26ho"" rel=""nofollow"">https:&#x2F;&#x2F;dev.to&#x2F;jordanholtdev&#x2F;how-to-create-a-web-page-with-h...</a><p>Cheers',
        33635218, 33635792, '2022-11-17 06:51:13', 'comment,author_javaunsafe2019,story_33635218', false),
       ('e5645a07-291c-438a-b1b0-3d362004f3c1', 33635218, 'account-5',
        'Ask HN: Good resource on writing web app with plain JavaScript/HTML/CSS',
        'I was in the same boat with wanting to just learn plain HTML&#x2F;CSS&#x2F;JavaScript.<p>Mozilla docs didn&#x27;t quite do it for me but I swear by W3SCHOOLS website.<p>I also found this guy&#x27;s website helpful: <a href=""https:&#x2F;&#x2F;gomakethings.com&#x2F;"" rel=""nofollow"">https:&#x2F;&#x2F;gomakethings.com&#x2F;</a><p>I think the author is on HN; he swears by plain JS.<p>For me I just wanted to write a simple cross platform app, I decided Dart&#x2F;Flutter was the way to go in the end.',
        33635218, 33635784, '2022-11-17 06:49:12', 'comment,author_account-5,story_33635218', false),
       ('a0638d0e-60fa-4cde-8415-65e9fc8bc79d', 33635218, 'galaxyLogic',
        'Ask HN: Good resource on writing web app with plain JavaScript/HTML/CSS',
        'I remember when Java EE came about. They had something called  &quot;Petstore Demo&quot; running, and documented in articles.  Isn&#x27;t there anything like that for Node.js&#x2F;js&#x2F;html&#x2F;css ?',
        33635218, 33635680, '2022-11-17 06:29:35', 'comment,author_galaxyLogic,story_33635218', false),
       ('3815fea9-82a9-424b-9f9f-fef47d6fb8e7', 33635218, 'statico',
        'Ask HN: Good resource on writing web app with plain JavaScript/HTML/CSS',
        'I purposely wrote my home page (<a href=""https:&#x2F;&#x2F;langworth.com"" rel=""nofollow"">https:&#x2F;&#x2F;langworth.com</a>) with plain, vanilla JavaScript, which features both WebGL and plaintext clients for a server-side text adventure game, because I wanted to remind myself that it&#x27;s not that difficult to build something interactive and fun without miles of modern front-end tooling.<p>It started as a single page but I eventually broke it out into a few files for organization. The WebGL parts are lengthy and boilerplate-y because, well, that&#x27;s GL for you. <a href=""https:&#x2F;&#x2F;github.com&#x2F;statico&#x2F;langterm"" rel=""nofollow"">https:&#x2F;&#x2F;github.com&#x2F;statico&#x2F;langterm</a>',
        33635218, 33635665, '2022-11-17 06:27:14', 'comment,author_statico,story_33635218', false);

