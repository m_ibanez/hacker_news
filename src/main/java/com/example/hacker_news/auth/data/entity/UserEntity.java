package com.example.hacker_news.auth.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "user", schema = "hacker_news")
@ToString
public class UserEntity {

    @Id
    private String username;
    private String password;
    private LocalDateTime createdAt;

}
