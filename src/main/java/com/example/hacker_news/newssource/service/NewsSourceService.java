package com.example.hacker_news.newssource.service;

import com.example.hacker_news.newssource.model.response.NewsResponseWrapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestOperations;

@Service
@RequiredArgsConstructor
public class NewsSourceService {

    @Value("${hacker-news.api-endpoint}")
    private String apiPath;

    private final RestOperations restOperations;

    public NewsResponseWrapper getLatestNews() {
        return restOperations.getForObject(apiPath, NewsResponseWrapper.class);
    }

}
