package com.example.hacker_news.news.api;

import com.example.hacker_news.news.model.request.NewsQueryParams;
import com.example.hacker_news.news.model.response.NewsPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Api(value = "News API", tags = "News")
@Validated
@RequestMapping("/news")
public interface NewsApi {

    @ApiOperation(
            value = "GET paginated news filtered by params",
            tags = "News",
            authorizations = {
                    @Authorization(value = "JWT")
            }
    )
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    NewsPage getNews(@NotNull @Valid NewsQueryParams params);

    @ApiOperation(
            value = "DELETE new by id", tags = "News",
            authorizations = {
                    @Authorization(value = "JWT")
            }
    )
    @DeleteMapping("/{newsId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void delete(@PathVariable @NotNull UUID newsId);

}
