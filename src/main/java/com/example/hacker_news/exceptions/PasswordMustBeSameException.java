package com.example.hacker_news.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class PasswordMustBeSameException extends ResponseStatusException {
    public PasswordMustBeSameException() {
        super(HttpStatus.UNPROCESSABLE_ENTITY, "Passwords must be the same");
    }
}
