package com.example.hacker_news.common.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@Api(hidden = true)
@Controller
public class HomeController {

    @ApiOperation(value = "GET home and redirect to Swagger documentation", hidden = true)
    @GetMapping("/")
    public String redirectHome() {
        return "redirect:/swagger-ui/";
    }

}
