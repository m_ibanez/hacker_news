package com.example.hacker_news.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.UUID;

public class NewsNotFoundException extends ResponseStatusException {
    public NewsNotFoundException(UUID newsId) {
        super(HttpStatus.NOT_FOUND, String.format("Could not found new with ID: %s", newsId.toString()));
    }
}
