package com.example.hacker_news.news.model.request;

import com.example.hacker_news.news.validator.annotation.ValidEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.Month;

@Data
public class NewsQueryParams {
    @ApiParam(value = "author", example = "gal_anonym")
    private String author;
    @ApiParam(value = "tags", example = "author_gal_anonym")
    private String tags;
    @ApiParam(value = "title", example = "Ask HN: Good resource")
    private String title;
    @ApiParam(value = "month", example = "november", allowableValues = "JANUARY,FEBRUARY,MARCH,APRIL,MAY,JUNE,JULY,AUGUST,SEPTEMBER,OCTOBER,NOVEMBER,DECEMBER")
    @ValidEnum(enumClass = Month.class)
    private String month;
    @ApiParam(value = "page", example = "1")
    @NotNull
    @Min(value = 1, message = "The minimum page number must be 1")
    private Integer page;
}
